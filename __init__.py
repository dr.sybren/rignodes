# SPDX-License-Identifier: GPL-2.0-or-later

bl_info = {
    "name": "RigNodes",
    "author": "Sybren & Nathan",
    "description": "ControlRig like thing",
    "blender": (3, 6, 0),
    "version": (0, 1, 0),
    "location": "Custom node editor",
    "warning": "Prototype",
    "category": "Animation",
    "support": "COMMUNITY",
}

is_first_load = "nodes" not in locals()
from . import execute, nodes, operators, gui

if not is_first_load:
    import importlib

    nodes = importlib.reload(nodes)
    execute = importlib.reload(execute)
    operators = importlib.reload(operators)
    gui = importlib.reload(gui)


def register() -> None:
    nodes.register()
    execute.register()
    operators.register()
    gui.register()


def unregister() -> None:
    gui.unregister()
    operators.unregister()
    execute.unregister()
    nodes.unregister()
