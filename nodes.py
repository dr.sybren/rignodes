# SPDX-License-Identifier: GPL-2.0-or-later

import functools
from collections import deque
from copy import copy
from math import degrees, radians
from typing import TypeVar, Callable, Optional, Iterable

import bpy
import nodeitems_utils
from mathutils import Vector, Quaternion, Matrix, Euler

T = TypeVar("T")


class RigNodesNodeTree(bpy.types.NodeTree):
    """Control EVERYTHING"""

    bl_idname = "RigNodesNodeTree"
    bl_label = "RigNodes"
    bl_icon = "OUTLINER_DATA_ARMATURE"

    autorun: bpy.props.BoolProperty(name="Autorun")  # type: ignore
    is_running: bpy.props.BoolProperty(name="Is Running")  # type: ignore

    def run_event(self, depsgraph: bpy.types.Depsgraph, event_name: str) -> bool:
        """Execute the sub-tree starting at an event node.

        :return: whether the event ran to completion.
        """
        event_to_node_types: dict[str, type[AbstractRigNodesEventNode]] = {
            "FORWARD": ForwardSolveNode,
            "BACKWARD": BackwardSolveNode,
        }
        try:
            node_type = event_to_node_types[event_name]
        except KeyError:
            raise ValueError(f"unknown event name {event_name}") from None

        start_node = self._find_node_of_type(node_type)
        if not start_node:
            return False

        self.is_running = True
        try:
            self._prepare_nodes()
            self._run_from_node(depsgraph, start_node)
        finally:
            self.is_running = False

        return True

    def _find_forward_solve(self) -> "ForwardSolveNode":
        for node in self.nodes:
            if isinstance(node, ForwardSolveNode):
                return node
        raise ValueError("No ForwardSolveNode found in tree")

    def _find_node_of_type(
        self,
        nodeType: type["AbstractRigNodesEventNode"],
    ) -> Optional["AbstractRigNodesEventNode"]:
        for node in self.nodes:
            if isinstance(node, nodeType):
                return node
        return None

    def _prepare_nodes(self) -> None:
        for node in self.nodes:
            node.reset_run()

    def _run_from_node(
        self, depsgraph: bpy.types.Depsgraph, start_node: "AbstractRigNodesNode"
    ) -> None:
        # Execute the ForwardSolveNode and traverse its output connections
        to_visit = deque([start_node])
        visited = set()

        print(f"\033[38;5;214mRunning tree {self.name}\033[0m")
        while to_visit:
            node: AbstractRigNodesNode = to_visit[0]
            print(f"    - {node}")

            nodes_before = list(node.exec_order_prerequisites())
            if nodes_before:
                print(f"      {len(nodes_before)} prerequisites, going there first")
                # There are nodes to execute before this one. Push them to the front of the queue.
                to_visit.extendleft(reversed(nodes_before))
                continue

            # Everything that this node depends on has run, so time to run it
            # itself. It can be taken off the queue.
            to_visit.popleft()

            if node in visited:
                # This can happen when a node has inputs from two different
                # nodes; both of those will list this node as 'successor'.
                # raise ValueError(f"Already seen {node}, graph loops")
                continue
            visited.add(node)

            node.run(depsgraph)

            # Queue up the next nodes.
            nodes_after = list(node.exec_order_successors())
            to_visit.extend(nodes_after)
        print(f"\033[90mDone with {self.name}\033[0m")


class NodeSocketExecute(bpy.types.NodeSocket):
    """Execution flow"""

    bl_idname = "NodeSocketExecute"
    bl_label = "Execute"
    link_limit = 1

    def draw(self, context, layout, node, text):
        layout.label(text=text)

    def draw_color(self, context, node):
        return (1.0, 1.0, 1.0, 1.0)


class NodeSocketQuaternion(bpy.types.NodeSocketStandard):
    """Rotation represented by a quaternion"""

    bl_idname = "NodeSocketQuaternion"
    bl_label = "Rotation"

    default_value: bpy.props.FloatVectorProperty(  # type: ignore
        name="Quaternion",
        size=4,
        default=(1.0, 0.0, 0.0, 0.0),
        subtype="QUATERNION",
    )

    def draw(self, context, layout, node, text):
        layout.label(text=text)

    def draw_color(self, context, node):
        return (0.699, 0.281, 0.854, 1.0)


def _on_node_error_updated(
    self: "AbstractRigNodesNode", context: bpy.types.Context
) -> None:
    if not self.error:
        self.use_custom_color = False
        return
    self.use_custom_color = True
    self.color = (1, 0.47, 0.327)


class AbstractRigNodesNode(bpy.types.Node):
    bl_icon = "NONE"  # Default to having no icon.

    @classmethod
    def poll(cls, ntree):
        return ntree.bl_idname == RigNodesNodeTree.bl_idname

    has_run: bpy.props.BoolProperty(name="Has Run")  # type: ignore
    error: bpy.props.StringProperty(  # type: ignore
        name="Error",
        description="Error message; if empty, everything is fine",
        update=_on_node_error_updated,
    )

    def init(self, context: bpy.types.Context) -> None:
        pass

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        """Implement this in a subclass."""

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        if self.has_run:
            layout.label(text="just ran", icon="PLAY")
        else:
            layout.label(text="did not run", icon="PAUSE")

        if self.error:
            layout.label(text=self.error, icon="ERROR")

    def add_execution_sockets(self) -> None:
        self.add_execution_socket_input()
        self.add_execution_socket_output()

    def add_execution_socket_input(self) -> None:
        self.inputs.new(NodeSocketExecute.bl_idname, NodeSocketExecute.bl_label)

    def add_execution_socket_output(self) -> None:
        self.outputs.new(NodeSocketExecute.bl_idname, NodeSocketExecute.bl_label)

    def add_optional_input_socket(
        self, typename: str, label: str
    ) -> bpy.types.NodeSocket:
        sock = self.inputs.new(typename, label)
        sock.hide_value = True
        return sock

    def exec_order_prerequisites(self) -> Iterable["AbstractRigNodesNode"]:
        """Generator, yields the nodes that should be executed before this one."""
        # For input execution order, consider all connected nodes regardless of socket types.
        return self._connected_nodes(self.inputs, "from_socket")

    def exec_order_successors(self) -> Iterable["AbstractRigNodesNode"]:
        """Generator, yields the nodes that should be executed after this one."""

        # For output execution order, only consider actual 'Execute' sockets.
        def follow_socket(socket: bpy.types.NodeSocket) -> bool:
            return isinstance(socket, NodeSocketExecute) or isinstance(
                socket.node, AbstractAlwaysExecuteNode
            )

        return self._connected_nodes(self.outputs, "to_socket", follow_socket)

    def _connected_nodes(
        self,
        sockets: Iterable[bpy.types.NodeSocket],
        link_attr: str,
        socket_filter: Optional[Callable[[bpy.types.NodeSocket], bool]] = None,
    ) -> Iterable["AbstractRigNodesNode"]:
        """Generator, return connected rignodes nodes that haven't run yet.

        :param sockets: either self.inputs or self.outputs
        :param link_attr: either 'from_node' for inputs or 'to_node' for outputs.
        :param socket_filter: optional function that can determine which socket
            connections to follow (when returning True) or not (when returning
            False).
        """
        nodes_seen: set[AbstractRigNodesNode] = set()
        for socket in sockets:
            for link in socket.links:
                connected_socket = getattr(link, link_attr)
                if socket_filter is not None and not socket_filter(connected_socket):
                    continue
                connected_node = connected_socket.node
                if not isinstance(connected_node, AbstractRigNodesNode):
                    continue
                if connected_node in nodes_seen:
                    continue
                nodes_seen.add(connected_node)
                if not connected_node.has_run:
                    yield connected_node

    def run(self, depsgraph: bpy.types.Depsgraph) -> None:
        assert not self.has_run, "a node can only run once, reset it first"

        if self.mute:
            # Skip execution of this node, it's muted.
            self._first_input_to_output()
            self.has_run = True
            return

        try:
            self.execute(depsgraph)
        except Exception as ex:
            self.error = str(ex)
            raise
        finally:
            self.has_run = True

    def __str__(self) -> str:
        return f"{self.__class__.__name__}({self.name!r})"

    def reset_run(self) -> None:
        self.has_run = False
        self.error = ""

    def _get_input_socket_value(self, input_socket: bpy.types.NodeSocket) -> object:
        for link in input_socket.links:
            return link.from_socket.default_value
        return input_socket.default_value

    def _get_input_ptr(self, input_socket_name: str, expectType: type[T]) -> T | None:
        """Return the socket value, or None in some cases.

        None is returned only when the socket value is None, for example with
        empty ID pointer sockets. Hence the function name.
        """
        try:
            input_socket = self.inputs[input_socket_name]
        except KeyError:
            keys = ", ".join(self.inputs.keys())
            raise KeyError(
                f"socket {input_socket_name} does not exist, found only {keys}"
            )

        value = self._get_input_socket_value(input_socket)
        if value is None:
            return value
        if isinstance(value, expectType):
            return value
        convertedValue: T = expectType(value)  # type: ignore
        return convertedValue

    def _get_input_value(self, input_socket_name: str, expectType: type[T]) -> T:
        """Return the socket value.

        Same as `_get_input_ptr()` except that this function never returns
        `None`. This means it cannot be used for pointer types (which can be
        None), but can be useful for other values (like floats and vectors) that
        offer a UI for editing the default value for unconnected sockets, and
        thus always have a value.
        """
        value = self._get_input_ptr(input_socket_name, expectType)
        if value is None:
            raise TypeError(f"input socket {input_socket_name} was unexpectedly None")
        return value

    def _get_optional_input_value(
        self, input_socket_name: str, expectType: type[T]
    ) -> T | None:
        """Return the connected socket value, or None if not connected.

        Note that the 'optional' in the name indicates optional functionality.
        It does *not* refer to optionally being connected; that's always
        optional for any input socket.

        An example of optional functionality would be to only set a bone's
        rotation when the 'Rotation' socket is connected.
        """
        input_socket = self.inputs[input_socket_name]
        for link in input_socket.links:
            convertedValue = expectType(link.from_socket.default_value)  # type: ignore
            return convertedValue
        return None

    def _first_input_to_output(self) -> None:
        """Copy the first input's default value to the output, if the sockets are compatible."""

        if not self.inputs or not self.outputs:
            return

        input: bpy.types.NodeSocket = self.inputs[0]
        output: bpy.types.NodeSocket = self.outputs[0]

        if input.type != output.type:
            # Different types, cannot handle.
            return

        input_value = self._get_input_socket_value(input)

        try:
            output.default_value = input_value
            print(f"      (muted) sending {input_value} -> {output}")
        except ValueError:
            # If they are not compatible, just keep the output value as-is.
            pass

    def recreate(self, context: bpy.types.Context) -> None:
        def copy_value(value: object) -> object:
            if isinstance(value, (str, bytes)):
                return value
            if hasattr(value, "__iter__"):
                # The VSCode type checker doesn't understand that having
                # `__iter__` will make `list(value)` work.
                return list(value)  # type: ignore
            try:
                return copy(value)
            except TypeError:
                return value

        # Store the default values of the sockets.
        default_values_input = {
            sock.name: copy_value(sock.default_value)
            for sock in self.inputs
            if hasattr(sock, "default_value")
        }
        default_values_output = {
            sock.name: copy_value(sock.default_value)
            for sock in self.outputs
            if hasattr(sock, "default_value")
        }

        # Store the links.
        links_input = {
            sock.name: [link.from_socket for link in sock.links] for sock in self.inputs
        }
        links_output = {
            sock.name: [link.to_socket for link in sock.links] for sock in self.outputs
        }

        # Recreate the sockets.
        self.inputs.clear()
        self.outputs.clear()
        self.init(context)

        # Restore the default values.
        for name, value in default_values_input.items():
            sock = self.inputs.get(name)
            if not sock:
                continue
            sock.default_value = value
        for name, value in default_values_output.items():
            sock = self.outputs.get(name)
            if not sock:
                continue
            sock.default_value = value

        # Restore the links.
        tree = self.id_data
        for my_socket_name, from_sockets in links_input.items():
            to_socket = self.inputs.get(my_socket_name)
            if not to_socket:
                continue
            for from_socket in from_sockets:
                tree.links.new(from_socket, to_socket)
        for my_socket_name, to_sockets in links_output.items():
            from_socket = self.outputs.get(my_socket_name)
            if not from_socket:
                continue
            for to_socket in to_sockets:
                tree.links.new(from_socket, to_socket)


class AbstractRigNodesEventNode(AbstractRigNodesNode):
    """Node that can only exist once in a tree"""


class AbstractAlwaysExecuteNode(AbstractRigNodesNode):
    """Node that always executes when is connected to any output of any executed node"""


class ForwardSolveNode(AbstractRigNodesEventNode):
    """Event node for the 'Forward Solve' event"""

    bl_idname = "ForwardSolveNode"
    bl_label = "Forward Solve"
    bl_icon = "PLAY"

    def init(self, context):
        self.add_execution_socket_output()


class BackwardSolveNode(AbstractRigNodesEventNode):
    """Event node for the 'Backward Solve' event"""

    bl_idname = "BackwardSolveNode"
    bl_label = "Backward Solve"
    bl_icon = "PLAY"

    def init(self, context):
        self.add_execution_socket_output()


_bone_head_tail = [
    ("HEAD", "Head", ""),
    ("TAIL", "Tail", ""),
]


class GetBoneNode(AbstractRigNodesNode):
    """Gets the transform of a bone, in world space"""

    bl_idname = "GetBoneNode"
    bl_label = "Get Bone"
    bl_icon = "BONE_DATA"

    head_tail: bpy.props.EnumProperty(name="Side", items=_bone_head_tail)  # type: ignore

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "head_tail")

    def init(self, context):
        self.inputs.new("NodeSocketObject", "Armature")
        self.inputs.new("NodeSocketString", "Bone")

        self.outputs.new("NodeSocketVector", "Location")
        self.outputs.new("NodeSocketQuaternion", "Rotation")
        self.outputs.new("NodeSocketVector", "Scale")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        arm_ob = self._get_input_ptr("Armature", bpy.types.Object)
        bone_name = self._get_input_value("Bone", str)
        if not arm_ob or not bone_name:
            return
        bone = arm_ob.pose.bones.get(bone_name)
        if not bone:
            return

        mat = bone.matrix.copy()
        match self.head_tail:
            case "HEAD":
                loc = bone.head
            case "TAIL":
                loc = bone.tail
            case _:
                loc = Vector((0, 0, 0))

        arm_eval: bpy.types.Object = arm_ob.evaluated_get(depsgraph)
        arm_matrix = arm_eval.matrix_world
        mat_world: Matrix = arm_matrix @ mat

        v_nil = Vector((0, 0, 0))
        bone_rest_rot_scale = bone.bone.matrix_local.copy()
        bone_rest_rot_scale.translation = v_nil

        _, rot, scale = (mat_world @ bone_rest_rot_scale.inverted()).decompose()

        self.outputs["Location"].default_value = arm_matrix @ loc
        self.outputs["Rotation"].default_value = rot
        self.outputs["Scale"].default_value = scale


_enum_control_space = [
    ("WORLD", "World", "Global space"),
    (
        "CHANNELS",
        "Local Channels",
        "The local Loc/Rot/Scale channel values, ignoring the delta transforms",
    ),
]


class GetControlNode(AbstractRigNodesNode):
    """Gets the transform of a control, in world space"""

    bl_idname = "GetControlNode"
    bl_label = "Get Control"
    bl_icon = "EMPTY_ARROWS"

    space: bpy.props.EnumProperty(  # type: ignore
        name="Space",
        items=_enum_control_space,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "space")

    def init(self, context: bpy.types.Context) -> None:
        self.inputs.new("NodeSocketObject", "Control")
        self.outputs.new("NodeSocketVector", "Location")
        self.outputs.new("NodeSocketQuaternion", "Rotation")
        self.outputs.new("NodeSocketVector", "Scale")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        control_obj = self._get_input_ptr("Control", bpy.types.Object)

        matrix = self._get_matrix(control_obj)
        loc, rot, scale = matrix.decompose()

        self.outputs["Location"].default_value = loc
        self.outputs["Rotation"].default_value = rot
        self.outputs["Scale"].default_value = scale

    def _get_matrix(self, control: bpy.types.Object) -> Matrix:
        match self.space:
            case "WORLD":
                return control.matrix_world
            case "CHANNELS":
                match control.rotation_mode:
                    case "QUATERNION":
                        rot = control.rotation_quaternion
                    case "AXIS_ANGLE":
                        rot = control.rotation_axis_angle
                    case _:
                        rot = control.rotation_euler
                return Matrix.LocRotScale(control.location, rot, control.scale)
            case _:
                return Matrix.Identity(4)


class SetControlNode(AbstractRigNodesNode):
    """Sets the transform of a control"""

    bl_idname = "SetControlNode"
    bl_label = "Set Control"
    bl_icon = "EMPTY_ARROWS"

    space: bpy.props.EnumProperty(  # type: ignore
        name="Space",
        items=_enum_control_space,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "space")

    def init(self, context):
        self.add_execution_sockets()

        self.add_optional_input_socket("NodeSocketVector", "Location")
        self.add_optional_input_socket("NodeSocketQuaternion", "Rotation")
        self.add_optional_input_socket("NodeSocketVector", "Scale")
        self.inputs.new("NodeSocketObject", "Control")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        control_location = self._get_optional_input_value("Location", Vector)
        control_rotation = self._get_optional_input_value("Rotation", Quaternion)
        control_scale = self._get_optional_input_value("Scale", Vector)
        control_obj = self._get_input_ptr("Control", bpy.types.Object)

        if not (control_location or control_rotation or control_scale):
            return
        if not control_obj:
            return

        match self.space:
            case "WORLD":
                loc, rot, scale = control_obj.matrix_world.decompose()
                if control_location is not None:
                    loc = control_location
                if control_rotation is not None:
                    rot = control_rotation
                if control_scale is not None:
                    scale = control_scale
                control_obj.matrix_world = Matrix.LocRotScale(loc, rot, scale)
            case "CHANNELS":
                if control_location is not None:
                    control_obj.location = control_location
                if control_rotation is not None:
                    match control_obj.rotation_mode:
                        case "QUATERNION":
                            control_obj.rotation_quaternion = control_rotation
                        case "AXIS_ANGLE":
                            control_obj.rotation_axis_angle = (
                                control_rotation.to_axis_angle()
                            )
                        case _:
                            control_obj.rotation_euler = control_rotation.to_euler(
                                control_obj.rotation_mode
                            )
                if control_scale is not None:
                    control_obj.scale = control_scale


class ToVector(AbstractRigNodesNode):
    bl_idname = "ToVector"
    bl_label = "To Vector"
    bl_icon = "EMPTY_ARROWS"

    def init(self, context):
        self.inputs.new("NodeSocketFloat", "X")
        self.inputs.new("NodeSocketFloat", "Y")
        self.inputs.new("NodeSocketFloat", "Z")
        self.outputs.new("NodeSocketVector", "Vector")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        x = self._get_input_value("X", float)
        y = self._get_input_value("Y", float)
        z = self._get_input_value("Z", float)
        self.outputs["Vector"].default_value = Vector((x, y, z))


class SplitVector(AbstractRigNodesNode):
    bl_idname = "SplitVector"
    bl_label = "Split Vector"
    bl_icon = "EMPTY_ARROWS"

    def init(self, context):
        self.inputs.new("NodeSocketVector", "Vector")
        self.outputs.new("NodeSocketFloat", "X")
        self.outputs.new("NodeSocketFloat", "Y")
        self.outputs.new("NodeSocketFloat", "Z")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        v = self._get_input_value("Vector", Vector)
        self.outputs["X"].default_value = v.x
        self.outputs["Y"].default_value = v.y
        self.outputs["Z"].default_value = v.z


class Distance(AbstractRigNodesNode):
    """Calculates distance between two points"""

    bl_idname = "Distance"
    bl_label = "Distance"
    bl_icon = "EMPTY_ARROWS"

    def init(self, context):
        self.inputs.new("NodeSocketVector", "U")
        self.inputs.new("NodeSocketVector", "V")
        self.outputs.new("NodeSocketFloat", "Float")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        u = self._get_input_value("U", Vector)
        v = self._get_input_value("V", Vector)
        self.outputs["Float"].default_value = (u - v).length


class NormalFromPoints(AbstractRigNodesNode):
    """Calculates normal from three points (plane)"""

    bl_idname = "NormalFromPoints"
    bl_label = "Normal from Points"
    bl_icon = "EMPTY_ARROWS"

    def init(self, context):
        self.inputs.new("NodeSocketVector", "U")
        self.inputs.new("NodeSocketVector", "V")
        self.inputs.new("NodeSocketVector", "W")
        self.outputs.new("NodeSocketVector", "Result")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        u = self._get_input_value("U", Vector)
        v = self._get_input_value("V", Vector)
        w = self._get_input_value("W", Vector)
        a = v - u
        b = w - u
        normal = a.cross(b).normalized()
        self.outputs["Result"].default_value = normal


_enum_up_axis_items = (
    ("X", "X", ""),
    ("Y", "Y", ""),
    ("Z", "Z", ""),
)

_enum_track_axis_items = (
    ("X", "X", ""),
    ("Y", "Y", ""),
    ("Z", "Z", ""),
    ("-X", "-X", ""),
    ("-Y", "-Y", ""),
    ("-Z", "-Z", ""),
)


class RotateTowards(AbstractRigNodesNode):
    """Calculate the rotation from a vector with a track and up axis"""

    bl_idname = "RotateTowards"
    bl_label = "Rotate Towards"
    bl_icon = "EMPTY_ARROWS"

    track: bpy.props.EnumProperty(  # type: ignore
        name="Track", items=_enum_track_axis_items, default="Z"
    )

    up: bpy.props.EnumProperty(  # type: ignore
        name="Up", items=_enum_up_axis_items, default="Y"
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "up")
        layout.prop(self, "track")

    def init(self, context):
        self.inputs.new("NodeSocketVector", "Vector")
        self.inputs.new("NodeSocketVector", "RotateTo")
        self.outputs.new("NodeSocketQuaternion", "Rotation")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        origin = self._get_input_value("Vector", Vector)
        destination = self._get_input_value("RotateTo", Vector)
        vec = Vector((destination - origin))
        rot = vec.to_track_quat(self.track, self.up)
        self.outputs["Rotation"].default_value = rot.normalized()


class OffsetRotation(AbstractRigNodesNode):
    """Offset a rotation"""

    bl_idname = "OffsetRotation"
    bl_label = "Offset Rotation"
    bl_icon = "EMPTY_ARROWS"

    def init(self, context):
        self.inputs.new("NodeSocketQuaternion", "Base")
        self.inputs.new("NodeSocketQuaternion", "Offset")
        self.outputs.new("NodeSocketQuaternion", "Rotation")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        base = self._get_input_value("Base", Quaternion)
        offset = self._get_input_value("Offset", Quaternion)
        self.outputs["Rotation"].default_value = base @ offset


class MapRange(AbstractRigNodesNode):
    bl_idname = "MapRange"
    bl_label = "Map Range"
    bl_icon = "EMPTY_ARROWS"

    def init(self, context):
        self.inputs.new("NodeSocketFloat", "Value")
        self.inputs.new("NodeSocketFloat", "From Min")
        self.inputs.new("NodeSocketFloat", "From Max")
        self.inputs.new("NodeSocketFloat", "To Min")
        self.inputs.new("NodeSocketFloat", "To Max")
        self.outputs.new("NodeSocketFloat", "Result")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        val = self._get_input_value("Value", float)
        fmin = self._get_input_value("From Min", float)
        fmax = self._get_input_value("From Max", float)
        tmin = self._get_input_value("To Min", float)
        tmax = self._get_input_value("To Max", float)

        factor = (tmax - tmin) / (fmax - fmin)
        offset = tmin - factor * fmin
        self.outputs["Result"].default_value = factor * val + offset


class AngleFromVectors(AbstractRigNodesNode):
    """Calculate the angle between two vectors. Output in degrees"""

    bl_idname = "AngleFromVectors"
    bl_label = "Angle From Vectors"
    bl_icon = "EMPTY_ARROWS"

    def init(self, context):
        self.inputs.new("NodeSocketVector", "U")
        self.inputs.new("NodeSocketVector", "V")
        self.outputs.new("NodeSocketFloat", "Angle")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        u = self._get_input_value("U", Vector)
        v = self._get_input_value("V", Vector)

        angle = 0
        if u.length_squared > 0 and v.length_squared > 0:
            angle = u.angle(v)

        self.outputs["Angle"].default_value = degrees(angle)


_enum_vector_math_operations = [
    ("ADD", "Add", ""),
    ("SUBSTRACT", "Substract", ""),
    ("MULTIPLY", "Mutliply", ""),
    ("DIVIDE", "Divide", ""),
    ("CROSS", "Cross", ""),
]


class VectorMath(AbstractRigNodesNode):
    bl_idname = "VectorMath"
    bl_label = "Vector Math"
    bl_icon = "EMPTY_ARROWS"

    operation: bpy.props.EnumProperty(  # type: ignore
        name="Operation",
        items=_enum_vector_math_operations,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "operation")

    def init(self, context):
        self.inputs.new("NodeSocketVector", "U")
        self.inputs.new("NodeSocketVector", "V")
        self.outputs.new("NodeSocketVector", "Result")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        u = self._get_input_value("U", Vector)
        v = self._get_input_value("V", Vector)

        match self.operation:
            case "ADD":
                res = u + v
            case "MULTIPLY":
                res = u * v
            case "SUBSTRACT":
                res = u - v
            case "DIVIDE":
                res = Vector((x / y if y != 0.0 else 0.0 for x, y in zip(u, v)))
            case "CROSS":
                res = u.cross(v)
            case _:
                raise ValueError(f"Vector math operation not found: {self.operation}\n")

        self.outputs["Result"].default_value = res


_enum_math_operations = [
    ("ADD", "Add", ""),
    ("SUBSTRACT", "Substract", ""),
    ("MULTIPLY", "Mutliply", ""),
    ("DIVIDE", "Divide", ""),
]


class Math(AbstractRigNodesNode):
    bl_idname = "Math"
    bl_label = "Math"
    bl_icon = "EMPTY_ARROWS"

    operation: bpy.props.EnumProperty(  # type: ignore
        name="Operation",
        items=_enum_math_operations,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "operation")

    def init(self, context):
        self.inputs.new("NodeSocketFloat", "U")
        self.inputs.new("NodeSocketFloat", "V")
        self.outputs.new("NodeSocketFloat", "Result")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        u = self._get_input_value("U", float)
        v = self._get_input_value("V", float)

        match self.operation:
            case "ADD":
                res = u + v
            case "MULTIPLY":
                res = u * v
            case "SUBSTRACT":
                res = u - v
            case "DIVIDE":
                res = u / v if v != 0 else 0
            case _:
                raise ValueError(f"Math operation not found: {self.operation}\n")

        self.outputs["Result"].default_value = res


class SetCursorNode(AbstractAlwaysExecuteNode):
    """Sets the location and/or rotation of the 3D cursor"""

    bl_idname = "SetCursorNode"
    bl_label = "Set Cursor"
    bl_icon = "CURSOR"

    def init(self, context):
        self.add_execution_sockets()
        self.add_optional_input_socket("NodeSocketVector", "Location")
        self.add_optional_input_socket("NodeSocketQuaternion", "Rotation")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        control_location = self._get_optional_input_value("Location", Vector)
        control_rotation = self._get_optional_input_value("Rotation", Quaternion)

        cursor = depsgraph.scene.cursor
        if control_location is not None:
            cursor.location = control_location
        if control_rotation is not None:
            cursor.rotation_mode = "QUATERNION"
            cursor.rotation_quaternion = control_rotation


class SetBoneNode(AbstractRigNodesNode):
    """Sets the transform of a bone"""

    bl_idname = "SetBoneNode"
    bl_label = "Set Bone"
    bl_icon = "BONE_DATA"

    space: bpy.props.EnumProperty(  # type: ignore
        name="Space",
        items=_enum_control_space,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "space")

    def init(self, context):
        self.add_execution_sockets()

        self.add_optional_input_socket("NodeSocketVector", "Location")
        self.add_optional_input_socket("NodeSocketQuaternion", "Rotation")
        self.add_optional_input_socket("NodeSocketVector", "Scale")
        self.inputs.new("NodeSocketObject", "Armature")
        self.inputs.new("NodeSocketString", "Bone")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        control_location = self._get_optional_input_value("Location", Vector)
        control_rotation = self._get_optional_input_value("Rotation", Quaternion)
        control_scale = self._get_optional_input_value("Scale", Vector)

        if not (control_location or control_rotation or control_scale):
            return

        arm_ob = self._get_input_ptr("Armature", bpy.types.Object)
        bone_name = self._get_input_ptr("Bone", str)

        if not arm_ob or not bone_name:
            return

        # Set the transform of the bone
        bone = arm_ob.pose.bones.get(bone_name)
        if not bone:
            return

        arm_eval: bpy.types.Object = arm_ob.evaluated_get(depsgraph)
        arm_matrix = arm_eval.matrix_world
        bone_mat_world: Matrix = arm_matrix @ bone.matrix

        loc, rot, scale = bone_mat_world.decompose()
        if control_location is not None:
            loc = control_location
        if control_rotation is not None:
            rot = control_rotation
        if control_scale is not None:
            scale = control_scale

        # TODO: Fix jittering bone scale which happens
        # esp. when multiple bones are parented to the rotated bone
        # rounding helps but does not entirely fix the issue.
        scale = [round(x, 4) for x in scale]
        v_nil = Vector((0, 0, 0))
        bone_rest_rot_scale = bone.bone.matrix_local.copy()

        match self.space:
            case "WORLD":
                bone_mat_world = Matrix.LocRotScale(loc, rot, scale)
                loc, rot, scale = bone_mat_world.decompose()
            case "CHANNELS":
                bone_rest_rot_scale.translation = v_nil
                mat_rot_scale = (
                    Matrix.LocRotScale(v_nil, rot, scale) @ bone_rest_rot_scale
                )
                mat_loc = Matrix.Translation(loc)
                bone_mat_world = mat_loc @ mat_rot_scale

        bone.matrix = arm_matrix.inverted() @ bone_mat_world


class TwoBoneIKNode(AbstractRigNodesNode):
    bl_idname = "TwoBoneIKNode"
    bl_label = "Two-bone IK"
    bl_icon = "CON_KINEMATIC"

    # Set to True to remove the cleanup and keep the IK constraint and temporary empties.
    _debug = False

    def init(self, context):
        self.add_execution_sockets()

        self.inputs.new("NodeSocketObject", "Armature")
        self.inputs.new("NodeSocketString", "Bone")
        self.inputs.new("NodeSocketVector", "Target")
        self.add_optional_input_socket("NodeSocketVector", "Pole Target")
        self.inputs.new("NodeSocketFloatAngle", "Pole Angle")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        arm_ob = self._get_input_ptr("Armature", bpy.types.Object)
        if not arm_ob:
            return

        bone_name = self._get_input_ptr("Bone", str)
        bone_lower_arm = arm_ob.pose.bones.get(bone_name)
        if not bone_lower_arm:
            return
        bone_upper_arm = bone_lower_arm.parent
        if not bone_upper_arm:
            return

        target_ob = None
        pole_target_ob = None
        con_ik = None

        locator: Callable[[str, Vector], bpy.types.Object]
        if self._debug:
            locator = functools.partial(self._debug_locator, scene=depsgraph.scene)
        else:
            locator = self._locator

        try:
            target_loc = self._get_input_value("Target", Vector)
            target_ob = locator("temp-ik-target", target_loc)

            pole_target_loc = self._get_optional_input_value("Pole Target", Vector)
            if pole_target_loc is not None:
                pole_target_ob = locator("temp-ik-pole-target", pole_target_loc)

            try:
                # Reuse constraint from previous run. This can be useful for
                # debugging, where the 'finally' clause below is commented out
                # to inspect the scene.
                con_ik = bone_lower_arm.constraints["temp-IK"]
            except KeyError:
                con_ik = bone_lower_arm.constraints.new("IK")
                con_ik.name = "temp-IK"

            con_ik.target = target_ob
            con_ik.pole_target = pole_target_ob
            con_ik.pole_angle = self._get_input_value("Pole Angle", float)
            con_ik.chain_count = 2
            con_ik.use_stretch = False

            # Applying the constraint doesn't update the entire chain, so do that
            # ourselves instead.
            depsgraph.update()

            bone_upper_mat = bone_upper_arm.matrix
            bone_lower_mat = bone_lower_arm.matrix
        finally:
            if not self._debug:
                if con_ik:
                    bone_lower_arm.constraints.remove(con_ik)
                if target_ob:
                    bpy.data.objects.remove(target_ob)
                if pole_target_ob:
                    bpy.data.objects.remove(pole_target_ob)

        bone_upper_arm.matrix = bone_upper_mat
        bone_lower_arm.matrix = bone_lower_mat

    def _locator(self, name: str, location: Vector) -> bpy.types.Object:
        try:
            ob = bpy.data.objects[name]
        except KeyError:
            ob = bpy.data.objects.new(name, None)
        ob.location = location
        return ob

    def _debug_locator(
        self, name: str, location: Vector, scene: bpy.types.Scene
    ) -> bpy.types.Object:
        ob = self._locator(name, location)
        ob.empty_display_type = "ARROWS"
        ob.empty_display_size = 0.5

        try:
            scene.collection.objects.link(ob)
        except RuntimeError:
            pass  # Already in scene collection.

        return ob


_enum_euler_order = [
    ("XYZ", "XYZ", ""),
    ("XZY", "XZY", ""),
    ("YXZ", "YXZ", ""),
    ("YZX", "YZX", ""),
    ("ZXY", "ZXY", ""),
    ("ZYX", "ZYX", ""),
]


class ToEulerNode(AbstractRigNodesNode):
    """Quaternion to Euler conversion; output in degrees"""

    bl_idname = "ToEulerNode"
    bl_label = "To Euler"
    bl_icon = "DRIVER_ROTATIONAL_DIFFERENCE"

    order: bpy.props.EnumProperty(  # type: ignore
        name="Order",
        items=_enum_euler_order,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "order")

    def init(self, context: bpy.types.Context) -> None:
        self.inputs.new("NodeSocketQuaternion", "Rotation")
        self.outputs.new("NodeSocketFloat", "X")
        self.outputs.new("NodeSocketFloat", "Y")
        self.outputs.new("NodeSocketFloat", "Z")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        rotation = self._get_input_value("Rotation", Quaternion)
        euler = rotation.to_euler(self.order)
        self.outputs["X"].default_value = degrees(euler.x)
        self.outputs["Y"].default_value = degrees(euler.y)
        self.outputs["Z"].default_value = degrees(euler.z)


class FromEulerNode(AbstractRigNodesNode):
    """Euler (in degrees) to Quaternion conversion"""

    bl_idname = "FromEulerNode"
    bl_label = "From Euler"
    bl_icon = "DRIVER_ROTATIONAL_DIFFERENCE"

    order: bpy.props.EnumProperty(  # type: ignore
        name="Order",
        items=_enum_euler_order,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "order")

    def init(self, context: bpy.types.Context) -> None:
        self.outputs.new("NodeSocketQuaternion", "Rotation")
        self.inputs.new("NodeSocketFloat", "X")
        self.inputs.new("NodeSocketFloat", "Y")
        self.inputs.new("NodeSocketFloat", "Z")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        x: float = self._get_input_value("X", float)
        y: float = self._get_input_value("Y", float)
        z: float = self._get_input_value("Z", float)

        euler = Euler((radians(x), radians(y), radians(z)), self.order)
        self.outputs["Rotation"].default_value = euler.to_quaternion()


class ClampNode(AbstractRigNodesNode):
    """Clamp a value between two other values"""

    bl_idname = "ClampNode"
    bl_label = "Clamp"
    bl_icon = "FCURVE"

    def init(self, context: bpy.types.Context) -> None:
        self.inputs.new("NodeSocketFloat", "Value")
        self.inputs.new("NodeSocketFloat", "Minimum")
        self.inputs.new("NodeSocketFloat", "Maximum")
        self.outputs.new("NodeSocketFloat", "Result")

    def execute(self, depsgraph: bpy.types.Depsgraph) -> None:
        value = self._get_input_value("Value", float)
        minimum = self._get_input_value("Minimum", float)
        maximum = self._get_input_value("Maximum", float)

        clamped = max(minimum, min(value, maximum))
        self.outputs["Result"].default_value = clamped


def _on_num_sockets_change(self: "SequenceNode", context: bpy.types.Context) -> None:
    self.recreate(context)


class SequenceNode(AbstractRigNodesNode):
    """Multiple 'Execute' node sockets"""

    bl_idname = "SequenceNode"
    bl_label = "Sequence"
    bl_icon = "STROKE"

    num_socks: bpy.props.IntProperty(  # type: ignore
        name="Number of Sockets",
        default=2,
        update=_on_num_sockets_change,
    )

    def draw_buttons(
        self, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        super().draw_buttons(context, layout)
        layout.prop(self, "num_socks")

    def init(self, context):
        self.add_execution_socket_input()
        for index in range(self.num_socks):
            self.outputs.new(
                NodeSocketExecute.bl_idname,
                f"{NodeSocketExecute.bl_label} {index+1}",
            )


class RigNodesNodeCategory(nodeitems_utils.NodeCategory):
    @classmethod
    def poll(cls, context):
        return context.space_data.tree_type == "RigNodesNodeTree"


class RigNodes_OT_rebuild_node(bpy.types.Operator):
    bl_idname = "rignodes.rebuild_node"
    bl_label = "Rebuild Node"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        active_node = getattr(context, "active_node", None)
        return (
            active_node is not None
            and active_node.select
            and hasattr(active_node, "recreate")
        )

    def execute(self, context: bpy.types.Context) -> set[str]:
        self.report(
            {"INFO"},
            f"Rebuilding {context.active_node.label or context.active_node.name!r}",
        )
        context.active_node.recreate(context)
        return {"FINISHED"}


node_categories = [
    RigNodesNodeCategory(
        "EVENTs",
        "Events",
        items=[
            nodeitems_utils.NodeItem("ForwardSolveNode"),
            nodeitems_utils.NodeItem("BackwardSolveNode"),
        ],
    ),
    RigNodesNodeCategory(
        "FLOW",
        "Flow",
        items=[
            nodeitems_utils.NodeItem("SequenceNode"),
        ],
    ),
    RigNodesNodeCategory(
        "CONTROL",
        "Control",
        items=[
            nodeitems_utils.NodeItem("GetControlNode"),
            nodeitems_utils.NodeItem("SetControlNode"),
        ],
    ),
    RigNodesNodeCategory(
        "BONE",
        "Bone",
        items=[
            nodeitems_utils.NodeItem("GetBoneNode"),
            nodeitems_utils.NodeItem("SetBoneNode"),
            nodeitems_utils.NodeItem("TwoBoneIKNode"),
        ],
    ),
    RigNodesNodeCategory(
        "MATH",
        "Math",
        items=[
            nodeitems_utils.NodeItem("RotateTowards"),
            nodeitems_utils.NodeItem("OffsetRotation"),
            nodeitems_utils.NodeItem("AngleFromVectors"),
            nodeitems_utils.NodeItem("NormalFromPoints"),
            nodeitems_utils.NodeItem("SplitVector"),
            nodeitems_utils.NodeItem("ToVector"),
            nodeitems_utils.NodeItem("Distance"),
            nodeitems_utils.NodeItem("MapRange"),
            nodeitems_utils.NodeItem("VectorMath"),
            nodeitems_utils.NodeItem("Math"),
            nodeitems_utils.NodeItem("ClampNode"),
            nodeitems_utils.NodeItem("ToEulerNode"),
            nodeitems_utils.NodeItem("FromEulerNode"),
        ],
    ),
    RigNodesNodeCategory(
        "DEBUG",
        "Debug",
        items=[
            nodeitems_utils.NodeItem("SetCursorNode"),
        ],
    ),
]


classes = (
    # The tree:
    RigNodesNodeTree,
    # Socket types:
    NodeSocketExecute,
    NodeSocketQuaternion,
    # Nodes:
    ForwardSolveNode,
    BackwardSolveNode,
    GetBoneNode,
    GetControlNode,
    SetBoneNode,
    SetControlNode,
    TwoBoneIKNode,
    SetCursorNode,
    SequenceNode,
    # Math Nodes
    RotateTowards,
    AngleFromVectors,
    OffsetRotation,
    NormalFromPoints,
    ToVector,
    SplitVector,
    Distance,
    MapRange,
    VectorMath,
    Math,
    ToEulerNode,
    FromEulerNode,
    ClampNode,
    # Operators:
    RigNodes_OT_rebuild_node,
)
_register, _unregister = bpy.utils.register_classes_factory(classes)


def register() -> None:
    _register()
    nodeitems_utils.register_node_categories("RIGNODES_NODES", node_categories)


def unregister() -> None:
    _unregister()
    nodeitems_utils.unregister_node_categories("RIGNODES_NODES")
