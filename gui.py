# SPDX-License-Identifier: GPL-2.0-or-later

import bpy


class RIGNODES_MT_snap_pie(bpy.types.Menu):
    bl_label = "Snap"

    def draw(self, _context):
        layout = self.layout
        pie = layout.menu_pie()

        pie.operator(
            "rignodes.snap_to_bone",
            text="Control to Bone",
            icon="EMPTY_ARROWS",
        )
        pie.operator(
            "view3d.snap_selected_to_grid",
            text="Selection to Grid",
            icon="RESTRICT_SELECT_OFF",
        )
        pie.operator(
            "view3d.snap_cursor_to_selected", text="Cursor to Selected", icon="CURSOR"
        )
        pie.operator(
            "view3d.snap_selected_to_cursor",
            text="Selection to Cursor",
            icon="RESTRICT_SELECT_OFF",
        ).use_offset = False
        pie.operator(
            "view3d.snap_selected_to_cursor",
            text="Selection to Cursor (Keep Offset)",
            icon="RESTRICT_SELECT_OFF",
        ).use_offset = True
        pie.operator(
            "view3d.snap_selected_to_active",
            text="Selection to Active",
            icon="RESTRICT_SELECT_OFF",
        )
        pie.operator(
            "view3d.snap_cursor_to_center", text="Cursor to World Origin", icon="CURSOR"
        )
        pie.operator(
            "view3d.snap_cursor_to_active", text="Cursor to Active", icon="CURSOR"
        )


def draw_nodetree_header_menu(self, context):
    tree = getattr(context.space_data, "edit_tree", None)
    if not tree or tree.bl_idname != "RigNodesNodeTree":
        return

    layout = self.layout
    row = layout.row(align=False)
    row.operator("rignodes.execute_tree", icon="PLAY")
    row.prop(tree, "autorun")
    row.prop(context.scene, "rignodes_mode")
    row.operator("rignodes.rebuild_node")


addon_keymaps = []
classes = (RIGNODES_MT_snap_pie,)
_register, _unregister = bpy.utils.register_classes_factory(classes)


def register() -> None:
    _register()
    bpy.types.NODE_MT_editor_menus.append(draw_nodetree_header_menu)

    # Add Shift+S for the RigNodes snapping menu:
    if not bpy.app.background:
        # Don't bother adding keymaps in background mode. The window manager is
        # there, but `wm.keyconfigs.addon` is None all of a sudden.
        wm = bpy.context.window_manager
        km = wm.keyconfigs.addon.keymaps.new(name="3D View", space_type="VIEW_3D")
        kmi = km.keymap_items.new("wm.call_menu_pie", "S", "PRESS", shift=True)
        kmi.properties.name = "RIGNODES_MT_snap_pie"
        addon_keymaps.append(km)


def unregister() -> None:
    _unregister()
    bpy.types.NODE_MT_editor_menus.remove(draw_nodetree_header_menu)

    # Remove the keyboard shortcut:
    if addon_keymaps:
        wm = bpy.context.window_manager
        for km in addon_keymaps:
            wm.keyconfigs.addon.keymaps.remove(km)
    addon_keymaps.clear()
